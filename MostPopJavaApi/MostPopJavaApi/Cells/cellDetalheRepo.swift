//
//  cellDetalheRepo.swift
//  MostPopJavaApi
//
//  Created by Bruno Garcia on 14/10/2017.
//  Copyright © 2017 Santos Brasil. All rights reserved.
//

import UIKit

class cellDetalheRepo: UITableViewCell {

    @IBOutlet weak var viewBackground: UIView!
    @IBOutlet weak var txtTitulo: UILabel!
    @IBOutlet weak var txtDetalhe: UILabel!
    @IBOutlet weak var txtOwner: UILabel!
    @IBOutlet weak var imgOwner: UIImageView!
    @IBOutlet weak var txtData: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

}
