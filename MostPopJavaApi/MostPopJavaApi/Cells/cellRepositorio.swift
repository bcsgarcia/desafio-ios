//
//  cellRepositorio.swift
//  MostPopJavaApi
//
//  Created by Bruno Garcia on 14/10/2017.
//  Copyright © 2017 Santos Brasil. All rights reserved.
//

import UIKit

class cellRepositorio: UITableViewCell {
    
    @IBOutlet weak var txtNomeRepositorio: UILabel!
    @IBOutlet weak var txtDescricaoRepositorio: UILabel!
    @IBOutlet weak var txtFork: UILabel!
    @IBOutlet weak var txtStar: UILabel!
    @IBOutlet weak var txtNome: UILabel!
    @IBOutlet weak var imgResponsavel: UIImageView!
    @IBOutlet weak var viewBackground: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

}
