//
//  vcCarregandoModal.swift
//  MostPopJavaApi
//
//  Created by Bruno Garcia on 13/10/2017.
//  Copyright © 2017 Santos Brasil. All rights reserved.
//

import UIKit

class vcCarregandoModal: UIViewController {

    var msg = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let strLabel = UILabel(frame: CGRect(x: 50, y: 0, width: 200, height: 50))
        strLabel.text = msg
        strLabel.textColor = UIColor.black
        let messageFrame = UIView(frame: CGRect(x: view.frame.midX - 90, y: view.frame.midY - 25 , width: 180, height: 50))
        messageFrame.layer.cornerRadius = 8
        messageFrame.backgroundColor = UIColor(white: 1, alpha: 0.9)
        let activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.gray)
        activityIndicator.frame = CGRect(x: 0, y: 0, width: 50, height: 50)
        activityIndicator.startAnimating()
        messageFrame.addSubview(activityIndicator)
        messageFrame.addSubview(strLabel)
        self.view.addSubview(messageFrame)
        let tap = UITapGestureRecognizer(target: self, action: #selector(vcCarregandoModal.handleTap(_:)))
        self.view.addGestureRecognizer(tap)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @objc func handleTap(_ sender: UITapGestureRecognizer) {
        self.view.removeFromSuperview()
    }

}
