//
//  vcWebView.swift
//  MostPopJavaApi
//
//  Created by Bruno Garcia on 14/10/2017.
//  Copyright © 2017 Santos Brasil. All rights reserved.
//

import UIKit

class vcWebView: UIViewController {

    @IBOutlet weak var webView: UIWebView!
    var url = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let localUrl = URL(string: url) {
            webView.loadRequest(NSURLRequest(url: localUrl) as URLRequest)
            
            
            
        }
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
