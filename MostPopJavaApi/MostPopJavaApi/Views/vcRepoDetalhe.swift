//
//  vcRepoDetalhe.swift
//  MostPopJavaApi
//
//  Created by Bruno Garcia on 14/10/2017.
//  Copyright © 2017 Santos Brasil. All rights reserved.
//

import UIKit

import Alamofire
import SwiftyJSON

class vcRepoDetalhe: baseViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var tbDetalheRepo: UITableView!
    
    var pullRequest = [PullRequest]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tbDetalheRepo.rowHeight = UITableViewAutomaticDimension
    }

    // MARK: - TableView -
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return pullRequest.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let i = indexPath.row
        let cell = self.tbDetalheRepo.dequeueReusableCell(withIdentifier: "cellPullRequest", for: indexPath) as! cellDetalheRepo
        cell.txtTitulo.text = pullRequest[i].titulo
        cell.txtDetalhe.text = pullRequest[i].corpo
        cell.txtOwner.text = pullRequest[i].usuario
        cell.imgOwner.downloadedFrom(link: pullRequest[i].usuarioAvatar!)
        cell.txtData.text = getData(data: pullRequest[i].data!)
        cell.viewBackground.setBordaArredondada()
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if let url = URL(string: pullRequest[indexPath.row].url!) {
            performSegue(withIdentifier: "segueOpenUrl", sender: indexPath)
        }
    }
    
    // MARK: - Carregando página do WebView -
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        var indexPath = sender as! IndexPath
        if (segue.identifier == "segueOpenUrl") {
            let tela = segue.destination as! vcWebView
            tela.url = pullRequest[indexPath.row].url!
        }
    }
    
    

}
