//
//  baseViewController.swift
//  MostPopJavaApi
//
//  Created by Bruno Garcia on 13/10/2017.
//  Copyright © 2017 Santos Brasil. All rights reserved.
//

import UIKit
import SwiftyJSON
import UserNotifications
import SystemConfiguration
import Alamofire
import AlamofireImage
import AlamofireObjectMapper

class baseViewController: UIViewController {

    
    override func viewDidLoad() {
        super.viewDidLoad()
        setBarStyle()
    }
    
    func showAlert(titulo:String, msg: String){
        let alert = UIAlertController(title: titulo, message: msg, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    func setBarStyle() {
        let nav = self.navigationController?.navigationBar
        nav?.barStyle = UIBarStyle.blackTranslucent
        nav?.tintColor = UIColor.white
        nav?.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
    }
    
    func carregando(msg:String?){
        let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "carregandoSID") as! vcCarregandoModal
        if let mensagem = msg {
            viewController.msg = mensagem
        }
        else {
            viewController.msg = "Aguarde"
        }
        
        self.addChildViewController(viewController)
        viewController.view.frame = self.view.frame
        viewController.view.alpha = 0
        self.view.addSubview(viewController.view)
        viewController.didMove(toParentViewController: self)
        
        UIView.animate(withDuration: 0.25, animations: {
            self.view.subviews[self.view.subviews.count-1].alpha = 1
        })
    }
    
    func fecharCarregando(){
        UIView.animate(withDuration: 0.25, animations: {
            self.view.subviews[self.view.subviews.count-1].alpha = 0
            self.view.subviews[self.view.subviews.count-1].removeFromSuperview()
        })
    }
    
    func abrirSobre(){
        let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "vcSobreSID") as! UINavigationController
        self.present(viewController, animated: true, completion: nil)
    }
    
    func getData(data: String) -> String {
        let arrDtSplit = data.characters.split{$0 == "T"}.map(String.init)
        let dtSplit = arrDtSplit[0].characters.split{$0 == "-"}.map(String.init)
        let dtRetorno = "\(dtSplit[2])/\(dtSplit[1])/\(dtSplit[0])"
        let hrSplit = arrDtSplit[1].characters.split{$0 == ":"}.map(String.init)
        let hrRetorno = "\(hrSplit[0]):\(hrSplit[1])"
        let dataRetorno = "\(dtRetorno) \(hrRetorno)"
        return dataRetorno
    }
    
    func getRepositorios(pagina:Int, completion: @escaping(([Repositorio]?) -> ()) ){
        let url = "https://api.github.com/search/repositories?q=language:Java&sort=stars&page=\(pagina)"
        Alamofire.request(url).responseArray(queue: nil, keyPath: "items", context: nil) { (response : DataResponse<[Repositorio]>) in
            switch (response.result){
            case .success(_):
                completion(response.result.value)
                break;
            case .failure(_):
                completion(nil)
                break;
            }
        }
    }
    
    func getPullRequest(usuario:String, repositorio:String, completion: @escaping(([PullRequest]?) -> ()) ){
        let url = "https://api.github.com/repos/\(usuario)/\(repositorio)/pulls"
        Alamofire.request(url).responseArray() { (response : DataResponse<[PullRequest]>) in
            switch (response.result){
            case .success(_):
                completion(response.result.value)
                break;
            case .failure(_):
                completion(nil)
                break;
            }
        }
    }
    
}



extension UIView {
    
    func setBordaArredondada(){
        self.layer.masksToBounds = true;
        self.layer.cornerRadius = 5.0;
        
        self.layer.borderWidth = 1
        self.layer.borderColor = UIColor(red:222/255.0, green:225/255.0, blue:227/255.0, alpha: 1.0).cgColor
        
        self.layer.shadowColor = UIColor.darkGray.cgColor
        self.layer.shadowOpacity = 0.6
        self.layer.shadowOffset = CGSize.zero
        self.layer.shadowRadius = 8
    }
    
    func setBordaArredondadaNotification(){
        self.layer.masksToBounds = true;
        self.layer.cornerRadius = 15.0;
        
        self.layer.borderWidth = 1
        self.layer.borderColor = UIColor(red:222/255.0, green:225/255.0, blue:227/255.0, alpha: 1.0).cgColor
    }
    
}

extension UIImageView {
    func downloadedFrom(url: URL, contentMode mode: UIViewContentMode = .scaleAspectFit) {
        contentMode = mode
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            guard
                let httpURLResponse = response as? HTTPURLResponse, httpURLResponse.statusCode == 200,
                let mimeType = response?.mimeType, mimeType.hasPrefix("image"),
                let data = data, error == nil,
                let image = UIImage(data: data)
                else { return }
            DispatchQueue.main.async() { () -> Void in
                self.image = image
            }
            }.resume()
    }
    
    func downloadedFrom(link: String, contentMode mode: UIViewContentMode = .scaleAspectFit) {
        guard let url = URL(string: link) else { return }
        self.clearImageFromCache(link: link)
        downloadedFrom(url: url, contentMode: mode)
    }
    
    func clearImageFromCache(link: String) {
        let URL = NSURL(string: link)!
        let URLRequest = NSURLRequest(url: URL as URL)
        let imageDownloader = UIImageView.af_sharedImageDownloader
        
        // Clear the URLRequest from the in-memory cache
        imageDownloader.imageCache?.removeImage(for: URLRequest as URLRequest, withIdentifier: nil)
        
        // Clear the URLRequest from the on-disk cache
        imageDownloader.sessionManager.session.configuration.urlCache?.removeCachedResponse(for: URLRequest as URLRequest)
    }
    
    
    
    
}
