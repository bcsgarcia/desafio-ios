//
//  vcJavaApis.swift
//  MostPopJavaApi
//
//  Created by Bruno Garcia on 14/10/2017.
//  Copyright © 2017 Santos Brasil. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import Font_Awesome_Swift

class vcRepoJava: baseViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet var mainView: UIView!
    @IBOutlet weak var tbDados: UITableView!
    
    var pagina : Int = 1
    let segueDetalhe = "segueDetalhe"
    var titulo = "Repositórios"

    var repositorios = [Repositorio]()
    var pullRequest = [PullRequest]()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = titulo
        tbDados.rowHeight = UITableViewAutomaticDimension
        buscaDadosRepositorios()
    }
    
    // MARK: - TableView -
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return repositorios.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let i = indexPath.row
        let cell = self.tbDados.dequeueReusableCell(withIdentifier: "cellItem", for: indexPath) as! cellRepositorio
        
        cell.txtNomeRepositorio.text = repositorios[i].nome
        cell.txtNome.text = repositorios[i].autor
        cell.txtDescricaoRepositorio.text = repositorios[i].descricao
        cell.txtFork.setFAText(prefixText: "", icon: .FACodeFork, postfixText: " \(repositorios[i].forks!)", size: 15)
        cell.txtStar.setFAText(prefixText: "", icon: .FAStar , postfixText: " \(repositorios[i].stars!)", size: 15)
        cell.imgResponsavel.downloadedFrom(link: repositorios[i].autorAvatar!)
        cell.viewBackground.setBordaArredondada()
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        buscaDetalheRepositorio(usuario: repositorios[indexPath.row].autor!, repositorio: repositorios[indexPath.row].nome!)
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let ultimo = repositorios.count - 1
        if indexPath.row == ultimo {
            pagina += 1
            buscaDadosRepositorios()
        }
    }
    
    // MARK: - buscando informações dos repositórios -
    func buscaDadosRepositorios(){
        carregando(msg: "Aguarde...")
        getRepositorios(pagina: pagina) { result in
            if let repo = result {
                self.repositorios.append(contentsOf: repo)
                self.tbDados.reloadData()
            }
            else {
                self.showAlert(titulo: "Atenção", msg: "Não foi possível carregar a requisição")
            }
            self.fecharCarregando()
        }
    }
    
    // MARK: - buscando informações dos pull request, se não retornar nada não carrega a próxima tela -
    func buscaDetalheRepositorio(usuario: String, repositorio: String){
        carregando(msg: "Aguarde...")
        getPullRequest(usuario: usuario, repositorio: repositorio ){ result in
            self.fecharCarregando()
            if let fullRequest = result {
                self.pullRequest.append(contentsOf: fullRequest)
                self.performSegue(withIdentifier: self.segueDetalhe, sender: repositorio);
            }
            else {
                self.showAlert(titulo: "Atenção", msg: "Não foi possível carregar a requisição")
            }
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if  segue.identifier == segueDetalhe
        {
            let destination = segue.destination as? vcRepoDetalhe
            destination!.pullRequest = pullRequest
            destination!.title = sender as? String
        }
    }
    
    @IBAction func btnSobreClick(_ sender: Any) {
        abrirSobre()
    }
    
    @IBAction func btnSairClick(_ sender: Any) {
        let refreshAlert = UIAlertController(title: "Atenção", message: "Você deseja sair?", preferredStyle: .actionSheet)
        refreshAlert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (action: UIAlertAction!) in
            exit(0)
        }))
        refreshAlert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (action: UIAlertAction!) in
            print("Handle Cancel Logic here")
        }))
        
        refreshAlert.popoverPresentationController?.sourceView = tbDados
        refreshAlert.popoverPresentationController?.sourceRect = tbDados.bounds
        
        present(refreshAlert, animated: true, completion: nil)
    }
}
