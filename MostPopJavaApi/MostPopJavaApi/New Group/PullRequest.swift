//
//  PullRequest.swift
//  MostPopJavaApi
//
//  Created by Bruno Garcia on 14/10/2017.
//  Copyright © 2017 Santos Brasil. All rights reserved.
//

import Foundation
import ObjectMapper

class PullRequest: Mappable {
    var titulo: String?
    var corpo: String?
    var usuario: String?
    var usuarioAvatar: String?
    var url: String?
    var data: String?
    
    required init?(map: Map){}
    
    func mapping(map: Map) {
        titulo          <- map["title"]
        corpo           <- map["body"]
        usuario         <- map["user.login"]
        usuarioAvatar   <- map["user.avatar_url"]
        url             <- map["html_url"]
        data            <- map["created_at"]
    }
    
}
