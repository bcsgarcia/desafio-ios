//
//  Repository.swift
//  MostPopJavaApi
//
//  Created by Bruno Garcia on 14/10/2017.
//  Copyright © 2017 Santos Brasil. All rights reserved.
//

import Foundation
import ObjectMapper

class Repositorio: Mappable {
    var nome: String?
    var autor: String?
    var autorAvatar : String?
    var descricao: String?
    var forks: Int?
    var stars: Int?
    
    required init?(map: Map) {}
    
    func mapping(map: Map) {
        nome        <- map["name"]
        descricao   <- map["description"]
        autor       <- map["owner.login"]
        autorAvatar <- map["owner.avatar_url"]
        stars       <- map["stargazers_count"]
        forks       <- map["forks_count"]
    }
    
}


